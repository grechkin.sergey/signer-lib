package ru.dmartynov.gost.security;

import org.w3c.dom.Node;
import org.w3c.dom.bootstrap.DOMImplementationRegistry;
import org.w3c.dom.ls.DOMImplementationLS;
import org.w3c.dom.ls.LSSerializer;
import org.xml.sax.InputSource;
import ru.dmartynov.gost.security.signers.SOAPSigner;

import javax.xml.namespace.QName;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.soap.SOAPMessage;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.handler.soap.SOAPHandler;
import javax.xml.ws.handler.soap.SOAPMessageContext;
import java.io.ByteArrayOutputStream;
import java.io.StringReader;
import java.util.Set;

/**
 * Created by dmartynov on 05.05.16.
 */
public class SignHandler implements SOAPHandler<SOAPMessageContext> {
    private final SOAPSigner soapSigner;
    private boolean debug = false;

    public SignHandler(SOAPSigner soapSigner) {
        this.soapSigner = soapSigner;
    }

    public SignHandler(SOAPSigner soapSigner, boolean debug) {
        this.soapSigner = soapSigner;
        this.debug = debug;
    }


    @Override
    public Set<QName> getHeaders() {
        return null;
    }

    @Override
    public boolean handleMessage(SOAPMessageContext context) {
        //Подписываем только исходящие сообщения
        if (Boolean.FALSE.equals(context.get(MessageContext.MESSAGE_OUTBOUND_PROPERTY))) {
            if (debug) {
                System.out.println("-------------------------------------------IN-------------------------------------------");
                printMessage(context.getMessage());
                System.out.println("----------------------------------------------------------------------------------------");
            }
            return true;
        }
        SOAPMessage signedMessage = soapSigner.signMessage(context.getMessage());
        context.setMessage(signedMessage);


        if (debug) {
            System.out.println("-------------------------------------------OUT-------------------------------------------");
            printMessage(context.getMessage());
            System.out.println("-----------------------------------------------------------------------------------------");
        }
        return true;

    }

    private void printMessage(SOAPMessage message) {
        try {
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            message.writeTo(bos);
            System.out.println(/*format*/(new String(bos.toByteArray())));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public boolean handleFault(SOAPMessageContext context) {
        if (debug) {
            System.out.println("----------------------------------------ERROR OUT----------------------------------------");
            printMessage(context.getMessage());
            System.out.println("-----------------------------------------------------------------------------------------");
        }
        return true;
    }

    @Override
    public void close(MessageContext context) {

    }

    public String format(String xml) {

        try {
            final InputSource src = new InputSource(new StringReader(xml));
            final Node document = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(src).getDocumentElement();
            final Boolean keepDeclaration = Boolean.valueOf(xml.startsWith("<?xml"));

            //May need this: System.setProperty(DOMImplementationRegistry.PROPERTY,"com.sun.org.apache.xerces.internal.dom.DOMImplementationSourceImpl");


            final DOMImplementationRegistry registry = DOMImplementationRegistry.newInstance();
            final DOMImplementationLS impl = (DOMImplementationLS) registry.getDOMImplementation("LS");
            final LSSerializer writer = impl.createLSSerializer();

            writer.getDomConfig().setParameter("format-pretty-print", Boolean.TRUE); // Set this to true if the output needs to be beautified.
            writer.getDomConfig().setParameter("xml-declaration", keepDeclaration); // Set this to true if the declaration is needed to be outputted.

            return writer.writeToString(document);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
