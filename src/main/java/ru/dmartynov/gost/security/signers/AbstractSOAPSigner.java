package ru.dmartynov.gost.security.signers;

public abstract class AbstractSOAPSigner extends CAPISigner implements SOAPSigner {
    public AbstractSOAPSigner(String gost, String container, String password) {
        super(gost, container, password);
    }
}
