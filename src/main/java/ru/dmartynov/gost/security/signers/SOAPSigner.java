package ru.dmartynov.gost.security.signers;

import javax.xml.soap.SOAPMessage;

/**
 * Created by dmartynov on 03.06.16.
 */
public interface SOAPSigner {
    SOAPMessage signMessage(SOAPMessage soapMessage);
}
