package ru.dmartynov.gost.security.signers;

import com.sun.jna.Native;
import com.sun.jna.Pointer;
import com.sun.jna.ptr.IntByReference;
import com.sun.jna.ptr.PointerByReference;
import ru.dmartynov.gost.security.nativ.Libcapi20;
import ru.dmartynov.gost.security.nativ.structures.CERT_PUBLIC_KEY_INFO;
import ru.dmartynov.gost.security.nativ.structures.PCERT_CONTEXT;

import java.util.Arrays;


/**
 * Created by dmartynov on 03.06.16.
 */
public class CAPISigner {

    static {
        System.setProperty("jna.library.path", "/opt/cprocsp/lib/amd64/");
    }

    private final String container;
    private final String password;

    private PointerByReference providerReference1;

    private Libcapi20 inst = Libcapi20.INST;

    private String gost = "2001";

    private String CP_PROVIDER = "";
    private int CP_PROVIDER_TYPE = 0;
    private int ALG_SID_GR3411 = 0;

    // gost can be 2001 or 2012
    public CAPISigner(String gost, String container, String password) {
        this.gost = gost;

        if (gost.equals("2012")) {
            CP_PROVIDER = Libcapi20.CP_PROVIDER_2012;
            CP_PROVIDER_TYPE = Libcapi20.CP_PROVIDER_TYPE_2012;
            ALG_SID_GR3411 = Libcapi20.ALG_SID_GR3411_2012;
        } else if (gost.equals("2001")) {
            CP_PROVIDER = Libcapi20.CP_PROVIDER_2001;
            CP_PROVIDER_TYPE = Libcapi20.CP_PROVIDER_TYPE_2001;
            ALG_SID_GR3411 = Libcapi20.ALG_SID_GR3411_2001;
        } else {
            CP_PROVIDER = Libcapi20.CP_PROVIDER_2001;
            CP_PROVIDER_TYPE = Libcapi20.CP_PROVIDER_TYPE_2001;
            ALG_SID_GR3411 = Libcapi20.ALG_SID_GR3411_2001;
        }

        this.container = container;
        this.password= password;
    }

    public PointerByReference getHash(byte[] dataToHash) {
        PointerByReference hashReference = new PointerByReference();
        boolean hashCreated = inst.CryptCreateHash(getProvider(), Libcapi20.ALG_CLASS_HASH | ALG_SID_GR3411,
                null, 0, hashReference);
        if (!hashCreated) {
            throw new RuntimeException("Ошибка создания хэша 0x" + Integer.toHexString(Native.getLastError()));
        }

        if (!inst.CryptHashData(hashReference.getValue(), dataToHash, dataToHash.length, 0)) {
            throw new RuntimeException("Ошибка расчета хэша 0x" + Integer.toHexString(Native.getLastError()));
        }

        return hashReference;
    }

    public byte[] getHashValue(byte[] dataToHash) {
        PointerByReference hashReference = getHash(dataToHash);
        IntByReference hashSize = new IntByReference();
        if (!inst.CryptGetHashParam(hashReference.getValue(), Libcapi20.HP_HASHVAL, null, hashSize, 0))
            throw new RuntimeException("Ошибка получения размера хэша 0x" + Integer.toHexString(Native.getLastError()));
        byte[] hash = new byte[hashSize.getValue()];
        if (!inst.CryptGetHashParam(hashReference.getValue(), Libcapi20.HP_HASHVAL, hash, hashSize, 0))
            throw new RuntimeException("Ошибка хэшировния 0x" + Integer.toHexString(Native.getLastError()));
        return hash;
    }

    public byte[] signHash(PointerByReference hashReference) {
        IntByReference sigSize = new IntByReference();
        if (!inst.CryptSignHashA(
                hashReference.getValue(),
                Libcapi20.AT_KEYEXCHANGE,
                null,
                0,
                null,
                sigSize
        )) throw new RuntimeException("Ошибка получения размера подписи 0x" + Integer.toHexString(Native.getLastError()));

        byte[] sig = new byte[sigSize.getValue()];

        if (!inst.CryptSignHashA(
                hashReference.getValue(),
                Libcapi20.AT_KEYEXCHANGE,
                null,
                0,
                sig,
                sigSize
        )) throw new RuntimeException("Ошибка подписи 0x" + Integer.toHexString(Native.getLastError()));


        for (int i = 0; i < sig.length / 2; i++) {
            byte temp = sig[i];
            sig[i] = sig[sig.length - 1 - i];
            sig[sig.length - 1 - i] = temp;
        }

        return sig;
    }

    public byte[] getCertificate() {
        IntByReference pubSize = new IntByReference();
        if (!inst.CryptExportPublicKeyInfo(
                getProvider(),
                Libcapi20.AT_KEYEXCHANGE,
                Libcapi20.PKCS_7_ASN_ENCODING | Libcapi20.X509_ASN_ENCODING,
                null,
                pubSize
        )) throw new RuntimeException("Ошибка получения размера публичного ключа 0x" + Integer.toHexString(Native.getLastError()));

        CERT_PUBLIC_KEY_INFO.ByReference publicKeyInfo = new CERT_PUBLIC_KEY_INFO.ByReference(pubSize.getValue());

        if (!inst.CryptExportPublicKeyInfo(
                getProvider(),
                Libcapi20.AT_KEYEXCHANGE,
                Libcapi20.PKCS_7_ASN_ENCODING | Libcapi20.X509_ASN_ENCODING,
                publicKeyInfo,
                pubSize
        )) throw new RuntimeException("Ошибка получения публичного ключа 0x" + Integer.toHexString(Native.getLastError()));


        Pointer my = inst.CertOpenSystemStoreA(getProvider(), "My");
        PCERT_CONTEXT certContext = inst.CertFindCertificateInStore(my, Libcapi20.X509_ASN_ENCODING | Libcapi20.PKCS_7_ASN_ENCODING, 0,
                Libcapi20.CERT_FIND_PUBLIC_KEY, publicKeyInfo, null);

        byte[] certificate = new byte[certContext.certContext.cbCertEncoded];
        certContext.certContext.pbCertEncoded.read(0, certificate, 0, certificate.length);

        return certificate;
    }

    private Pointer getProvider() {
        if(this.providerReference1 != null)
            return this.providerReference1.getValue();
        PointerByReference initProviderReference = new PointerByReference();
        if (!inst.CryptAcquireContextA(initProviderReference, container.trim(), CP_PROVIDER,
                CP_PROVIDER_TYPE, Libcapi20.CRYPT_SILENT))
            throw new RuntimeException("Ошибка инициализации криптопровайдера 0x" + Integer.toHexString(Native.getLastError()));

        if (password != null && !password.isEmpty()) {
            byte[] passwd = Arrays.copyOf(password.getBytes(), password.getBytes().length + 1);
            passwd[passwd.length - 1] = 0;
            if (!inst.CryptSetProvParam(initProviderReference.getValue(), Libcapi20.PP_KEYEXCHANGE_PIN, passwd, 0))
                throw new RuntimeException("Некорректный пароль контейнера 0x" + Integer.toHexString(Native.getLastError()));
        }

        this.providerReference1 = initProviderReference;

        return this.providerReference1.getValue();
    }

    public void closeProvider() {
        if(this.providerReference1 != null) {
            if (inst.CryptReleaseContext(providerReference1, 0))
                System.err.println("Close provider error 0x" + Integer.toHexString(Native.getLastError()));
            else
                providerReference1 = null;
        }
    }

    public String getGost() {
        return gost;
    }
}
