package ru.dmartynov.gost.security.signers;

import com.sun.jna.ptr.PointerByReference;
import com.sun.org.apache.xerces.internal.impl.dv.util.Base64;
import nu.xom.*;
import nu.xom.canonical.Canonicalizer;

import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPMessage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

import static nu.xom.canonical.Canonicalizer.EXCLUSIVE_XML_CANONICALIZATION;

/**
 * Created by dmartynov on 19.05.16.
 */

public class SmevSoapSigner extends AbstractSOAPSigner {
    private final String DIG_NS = "http://www.w3.org/2000/09/xmldsig#";

    public SmevSoapSigner(String gost, String container, String password) {
        super(gost, container, password);
    }


    @Override
    public SOAPMessage signMessage(SOAPMessage message) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        try {
            message.writeTo(byteArrayOutputStream);

            Builder builder = new Builder();
            byte[] bytes = byteArrayOutputStream.toByteArray();
            Document sourceDocument = builder.build(new ByteArrayInputStream(bytes));

            ByteArrayOutputStream rootOS = new ByteArrayOutputStream();
            Canonicalizer rootCanonicalizer = new Canonicalizer(rootOS, EXCLUSIVE_XML_CANONICALIZATION);
            rootCanonicalizer.write(sourceDocument);

            Document document = builder.build(new ByteArrayInputStream(rootOS.toByteArray()));


            Nodes entities = document.query("//*[local-name()='Charge']");
            if (entities.size() > 0) {
                signEntity((Element) entities.get(0));
            }

            Element envelope = document.getRootElement();
            envelope.addNamespaceDeclaration("wsse", "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd");
            envelope.addNamespaceDeclaration("wsu", "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd");

            Element body = envelope.getFirstChildElement("Body", "http://schemas.xmlsoap.org/soap/envelope/");
            body.addAttribute(new Attribute("wsu:Id", "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd", "body"));

            ByteArrayOutputStream canonicalBodyOS = new ByteArrayOutputStream();
            Canonicalizer bodyCanonicalizer = new Canonicalizer(canonicalBodyOS, EXCLUSIVE_XML_CANONICALIZATION);
            bodyCanonicalizer.write(body);


            byte[] bodyHash = getHashValue(canonicalBodyOS.toByteArray());

            Element header = envelope.getFirstChildElement("Header", "http://schemas.xmlsoap.org/soap/envelope/");
            if (header == null) {
                header = new Element("soap:Header", "http://schemas.xmlsoap.org/soap/envelope/");
                envelope.insertChild(header, 0);
            }
            Element security = new Element("wsse:Security", "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd");
            security.addAttribute(new Attribute("soap:actor", "http://schemas.xmlsoap.org/soap/envelope/", "http://smev.gosuslugi.ru/actors/smev"));
            header.appendChild(security);

            Element binarySecurityToken = new Element("wsse:BinarySecurityToken",
                    "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd");
            binarySecurityToken.addAttribute(new Attribute("EncodingType",
                    "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-soap-message-security-1.0#Base64Binary"));
            binarySecurityToken.addAttribute(new Attribute("ValueType",
                    "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-x509-token-profile-1.0#X509v3"));
            binarySecurityToken.addAttribute(new Attribute("wsu:Id",
                    "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd", "SenderCertificate"));
            binarySecurityToken.appendChild(Base64.encode(getCertificate()));
            security.appendChild(binarySecurityToken);

            Element signature = new Element("Signature", DIG_NS);
            security.appendChild(signature);

            Element signedInfo = new Element("SignedInfo", DIG_NS);
            signature.appendChild(signedInfo);


            Element canonicalizationMethod = new Element("CanonicalizationMethod", DIG_NS);
            signedInfo.appendChild(canonicalizationMethod);
            canonicalizationMethod.addAttribute(new Attribute("Algorithm", EXCLUSIVE_XML_CANONICALIZATION));

            Element signatureMethod = new Element("SignatureMethod", DIG_NS);
            signedInfo.appendChild(signatureMethod);
            String signatureAlgoStr = "";
            if (getGost().equals("2001")) signatureAlgoStr = "http://www.w3.org/2001/04/xmldsig-more#gostr34102001-gostr3411";
            else if (getGost().equals("2012")) signatureAlgoStr = "urn:ietf:params:xml:ns:cpxmlsec:algorithms:gostr34102012-gostr34112012-256";
            signatureMethod.addAttribute(new Attribute("Algorithm", signatureAlgoStr));
            // <ds:SignatureMethod Algorithm="urn:ietf:params:xml:ns:cpxmlsec:algorithms:gostr34102012-gostr34112012-256">
            //signatureMethod.addAttribute(new Attribute("Algorithm", "http://www.w3.org/2001/04/xmldsig-more#gostr34102001-gostr3411"));
            //signatureMethod.addAttribute(new Attribute("Algorithm", "urn:ietf:params:xml:ns:cpxmlsec:algorithms:gostr34102012-gostr34112012-256"));

            Element reference = new Element("Reference", DIG_NS);
            signedInfo.appendChild(reference);
            reference.addAttribute(new Attribute("URI", "#body"));

            Element transforms = new Element("Transforms", DIG_NS);
            reference.appendChild(transforms);

            Element transform = new Element("Transform", DIG_NS);
            transforms.appendChild(transform);
            transform.addAttribute(new Attribute("Algorithm", EXCLUSIVE_XML_CANONICALIZATION));

            Element digestMethod = new Element("DigestMethod", DIG_NS);
            reference.appendChild(digestMethod);
            String digestAlgoStr = "";
            if (getGost().equals("2001")) digestAlgoStr = "http://www.w3.org/2001/04/xmldsig-more#gostr3411";
            else if (getGost().equals("2012")) digestAlgoStr = "urn:ietf:params:xml:ns:cpxmlsec:algorithms:gostr34112012-256";
            digestMethod.addAttribute(new Attribute("Algorithm", digestAlgoStr));
            //<ds:DigestMethod Algorithm="urn:ietf:params:xml:ns:cpxmlsec:algorithms:gostr34112012-256">
            //digestMethod.addAttribute(new Attribute("Algorithm", "http://www.w3.org/2001/04/xmldsig-more#gostr3411"));
            //digestMethod.addAttribute(new Attribute("Algorithm", "urn:ietf:params:xml:ns:cpxmlsec:algorithms:gostr34112012-256"));

            Element digestValue = new Element("DigestValue", DIG_NS);
            reference.appendChild(digestValue);
            digestValue.appendChild(Base64.encode(bodyHash));

            ByteArrayOutputStream canonicalizedSignedInfoOS = new ByteArrayOutputStream();
            Canonicalizer signedInfoCanonicalizer = new Canonicalizer(canonicalizedSignedInfoOS, EXCLUSIVE_XML_CANONICALIZATION);
            signedInfoCanonicalizer.write(signedInfo);

            PointerByReference signedInfoHashReference = getHash(canonicalizedSignedInfoOS.toByteArray());
            byte[] siSignature = signHash(signedInfoHashReference);

            Element signatureValue = new Element("SignatureValue", DIG_NS);
            signature.appendChild(signatureValue);
            signatureValue.appendChild(Base64.encode(siSignature));

            Element keyInfo = new Element("KeyInfo", DIG_NS);
            signature.appendChild(keyInfo);


            Element securityTokenReference = new Element("wsse:SecurityTokenReference",
                    "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd");
            keyInfo.appendChild(securityTokenReference);

            Element reference2 = new Element("wsse:Reference",
                    "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd");
            securityTokenReference.appendChild(reference2);
            reference2.addAttribute(new Attribute("URI", "#SenderCertificate"));
            reference2.addAttribute(new Attribute("ValueType", "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-x509-token-profile-1.0#X509v3"));

            return MessageFactory.newInstance().createMessage(message.getMimeHeaders(),
                    new ByteArrayInputStream(document.toXML().getBytes()));
        } catch (Exception e) {
            throw new RuntimeException("Ошибка подписи SOAP пакета", e);
        }finally {
            closeProvider();
        }
    }

    private void signEntity(Element element) throws IOException {
        Element signature = new Element("Signature", DIG_NS);

        Element signedInfo = new Element("SignedInfo", DIG_NS);
        signature.appendChild(signedInfo);

        Element canonicalizationMethod = new Element("CanonicalizationMethod", DIG_NS);
        signedInfo.appendChild(canonicalizationMethod);

        Element signatureMethod = new Element("SignatureMethod", DIG_NS);
        signedInfo.appendChild(signatureMethod);

        Element reference = new Element("Reference", DIG_NS);
        signedInfo.appendChild(reference);

        Element transforms = new Element("Transforms", DIG_NS);
        reference.appendChild(transforms);

        Element transform = new Element("Transform", DIG_NS);
        transforms.appendChild(transform);

        Element transformTwo = new Element("Transform", DIG_NS);
        transforms.appendChild(transformTwo);

        Element digestMethod = new Element("DigestMethod", DIG_NS);
        reference.appendChild(digestMethod);

        Element digestValue = new Element("DigestValue", DIG_NS);
        reference.appendChild(digestValue);

        Element signatureValue = new Element("SignatureValue", DIG_NS);
        signature.appendChild(signatureValue);

        Element keyInfo = new Element("KeyInfo", DIG_NS);
        signature.appendChild(keyInfo);


        Element x509Data = new Element("X509Data", DIG_NS);
        keyInfo.appendChild(x509Data);

        Element x509Certificate = new Element("X509Certificate", DIG_NS);
        x509Data.appendChild(x509Certificate);

        /* Element object = new Element("Object", DIG_NS);
        signature.appendChild(object);

       Element qualifyingProperties = new Element("QualifyingProperties", DIG_NS);
        object.appendChild(qualifyingProperties);

        Element signedProperties = new Element("SignedProperties", DIG_NS);
        qualifyingProperties.appendChild(signedProperties);

        Element signedSignatureProperties = new Element("SignedSignatureProperties", DIG_NS);
        signedProperties.appendChild(signedSignatureProperties);

        Element signedDataObjectProperties = new Element("SignedDataObjectProperties", DIG_NS);
        signedProperties.appendChild(signedDataObjectProperties);

        Element unSignedProperties = new Element("UnSignedProperties", DIG_NS);
        qualifyingProperties.appendChild(unSignedProperties);

        Element unsignedSignatureProperties = new Element("UnsignedSignatureProperties", DIG_NS);
        unSignedProperties.appendChild(unsignedSignatureProperties);*/


        canonicalizationMethod.addAttribute(new Attribute("Algorithm", EXCLUSIVE_XML_CANONICALIZATION));
        transformTwo.addAttribute(new Attribute("Algorithm", EXCLUSIVE_XML_CANONICALIZATION));

        transform.addAttribute(new Attribute("Algorithm", "http://www.w3.org/2000/09/xmldsig#enveloped-signature"));


        digestMethod.addAttribute(new Attribute("Algorithm", "http://www.w3.org/2001/04/xmldsig-more#gostr3411"));
        signatureMethod.addAttribute(new Attribute("Algorithm", "http://www.w3.org/2001/04/xmldsig-more#gostr34102001-gostr3411"));

        Attribute referenceURI = new Attribute("URI", "");
        reference.addAttribute(referenceURI);

        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        Canonicalizer bodyCanonicalizer = new Canonicalizer(bos, EXCLUSIVE_XML_CANONICALIZATION);
        bodyCanonicalizer.write(element.getDocument());


        byte[] entitySignature = getHashValue(bos.toByteArray());
        digestValue.appendChild(Base64.encode(entitySignature));

        ByteArrayOutputStream canonicalizedSignedInfoOS = new ByteArrayOutputStream();
        Canonicalizer signedInfoCanonicalizer = new Canonicalizer(canonicalizedSignedInfoOS, EXCLUSIVE_XML_CANONICALIZATION);
        signedInfoCanonicalizer.write(signedInfo);

        byte[] signatureInfoSignature = signHash(getHash(canonicalizedSignedInfoOS.toByteArray()));
        signatureValue.appendChild(Base64.encode(signatureInfoSignature));


        x509Certificate.appendChild(Base64.encode(getCertificate()));

        element.appendChild(signature);


    }
}
