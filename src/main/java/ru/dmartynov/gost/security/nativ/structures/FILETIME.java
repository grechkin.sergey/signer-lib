package ru.dmartynov.gost.security.nativ.structures;

import com.sun.jna.Structure;

import java.util.Arrays;
import java.util.List;

public class FILETIME extends Structure {
    public int dwLowDateTime;
    public int dwHighDateTime;

    public FILETIME() {
    }

    protected List<?> getFieldOrder() {
        return Arrays.asList("dwLowDateTime", "dwHighDateTime");
    }

    public static class ByReference extends FILETIME implements Structure.ByReference {
    }

    ;

    public static class ByValue extends FILETIME implements Structure.ByValue {
    }

    ;
}