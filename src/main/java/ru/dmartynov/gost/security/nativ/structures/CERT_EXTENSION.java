package ru.dmartynov.gost.security.nativ.structures;

import com.sun.jna.Structure;

import java.util.Arrays;
import java.util.List;

public class CERT_EXTENSION extends Structure {
    public String pszObjId;
    public boolean fCritical;
    public CRYPT_INTEGER_BLOB Value;

    public CERT_EXTENSION() {
    }

    protected List<?> getFieldOrder() {
        return Arrays.asList("pszObjId", "fCritical", "Value");
    }

    public static class ByReference extends CERT_EXTENSION implements Structure.ByReference {
    }

    ;
}