package ru.dmartynov.gost.security.nativ.structures;

import com.sun.jna.Pointer;
import com.sun.jna.Structure;

import java.util.Arrays;
import java.util.List;

public class CRYPT_INTEGER_BLOB extends Structure {
    public int cbData;
    public Pointer pbData;

    public CRYPT_INTEGER_BLOB() {
    }

    protected List<?> getFieldOrder() {
        return Arrays.asList("cbData", "pbData");
    }

    public static class ByReference extends CRYPT_INTEGER_BLOB implements Structure.ByReference {
    }
}