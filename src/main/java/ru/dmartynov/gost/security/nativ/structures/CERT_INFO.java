package ru.dmartynov.gost.security.nativ.structures;

import com.sun.jna.Structure;

import java.util.Arrays;
import java.util.List;

public class CERT_INFO extends Structure {
    public int dwVersion;
    public CRYPT_INTEGER_BLOB SerialNumber;
    public CRYPT_ALGORITHM_IDENTIFIER SignatureAlgorithm;
    public CRYPT_INTEGER_BLOB Issuer;
    public FILETIME NotBefore;
    public FILETIME NotAfter;
    public CRYPT_INTEGER_BLOB Subject;
    public CERT_PUBLIC_KEY_INFO SubjectPublicKeyInfo;
    public CRYPT_BIT_BLOB IssuerUniqueId;
    public CRYPT_BIT_BLOB SubjectUniqueId;
    public int cExtension;
    public CERT_EXTENSION.ByReference rgExtension;

    public CERT_INFO() {
    }

    protected List<?> getFieldOrder() {
        return Arrays.asList("dwVersion", "SerialNumber", "SignatureAlgorithm", "Issuer", "NotBefore", "NotAfter", "Subject", "SubjectPublicKeyInfo", "IssuerUniqueId", "SubjectUniqueId", "cExtension", "rgExtension");
    }

    public static class ByReference extends CERT_INFO implements Structure.ByReference {
    }

    ;
}