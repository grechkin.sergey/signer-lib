package ru.dmartynov.gost.security.nativ.structures;

import com.sun.jna.Pointer;
import com.sun.jna.Structure;

import java.util.Arrays;
import java.util.List;

public class CERT_CONTEXT extends Structure {
    public int dwCertEncodingType;
    public Pointer pbCertEncoded;
    public int cbCertEncoded;
    public CERT_INFO.ByReference pCertInfo;
    public Pointer hCertStore;

    public CERT_CONTEXT() {
    }

    protected List<?> getFieldOrder() {
        return Arrays.asList("dwCertEncodingType", "pbCertEncoded", "cbCertEncoded", "pCertInfo", "hCertStore");
    }

    public static class ByReference extends CERT_CONTEXT implements Structure.ByReference {
    }
}

