package ru.dmartynov.gost.security.nativ.structures;

import com.sun.jna.Structure;

import java.util.Arrays;
import java.util.List;

public class CERT_PUBLIC_KEY_INFO extends Structure {
    public CRYPT_ALGORITHM_IDENTIFIER Algorithm;
    public CRYPT_BIT_BLOB PublicKey;

    public CERT_PUBLIC_KEY_INFO() {
    }

    public CERT_PUBLIC_KEY_INFO(int size) {
        allocateMemory(size);
    }


    protected List<?> getFieldOrder() {
        return Arrays.asList("Algorithm", "PublicKey");
    }

    public static class ByReference extends CERT_PUBLIC_KEY_INFO implements Structure.ByReference {
        public ByReference(int size) {
            super(size);
        }

        public ByReference() {
            super();
        }
    }


}