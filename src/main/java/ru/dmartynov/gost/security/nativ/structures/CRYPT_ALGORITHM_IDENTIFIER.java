package ru.dmartynov.gost.security.nativ.structures;

import com.sun.jna.Structure;

import java.util.Arrays;
import java.util.List;

public class CRYPT_ALGORITHM_IDENTIFIER extends Structure {
    public String pszObjId;
    public CRYPT_INTEGER_BLOB Parameters;

    public CRYPT_ALGORITHM_IDENTIFIER() {
    }

    protected List<?> getFieldOrder() {
        return Arrays.asList("pszObjId", "Parameters");
    }

    public static class ByReference extends CRYPT_ALGORITHM_IDENTIFIER implements Structure.ByReference {
    }

    ;
}