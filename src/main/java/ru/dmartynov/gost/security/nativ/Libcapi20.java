package ru.dmartynov.gost.security.nativ;

import com.sun.jna.Library;
import com.sun.jna.Native;
import com.sun.jna.Pointer;
import com.sun.jna.ptr.IntByReference;
import com.sun.jna.ptr.PointerByReference;
import ru.dmartynov.gost.security.nativ.structures.CERT_PUBLIC_KEY_INFO;
import ru.dmartynov.gost.security.nativ.structures.PCERT_CONTEXT;

/**
 * Created by dmartynov on 04.05.16.
 */
public interface Libcapi20 extends Library {
    Libcapi20 INST = (Libcapi20) Native.loadLibrary("libcapi20", Libcapi20.class);

    String CP_PROVIDER_2001 = "Crypto-Pro GOST R 34.10-2001 KC1 CSP";
    int CP_PROVIDER_TYPE_2001 = 75;
    int ALG_SID_GR3411_2001 = 30;

    String CP_PROVIDER_2012 = "Crypto-Pro GOST R 34.10-2012 KC1 CSP";
    //String GOST3411_2012_256 = "GOST3411_2012_256";
    //String CP_PROVIDER = "GOST3411_2012_256withGOST3410DH_2012_256";
    //String GOST3411_2012_512 = "GOST3411_2012_512";
    //String GOST3411_2012_512WITH_GOST3410DH_2012_512 = "GOST3411_2012_512withGOST3410DH_2012_512";
    int CP_PROVIDER_TYPE_2012 = 80;
    int ALG_SID_GR3411_2012 = 33;

    int CRYPT_VERIFYCONTEXT = 0xF0000000;

    int PP_ENUMCONTAINERS = 2;
    int CRYPT_FIRST = 1;
    int CRYPT_NEXT = 2;
    int CRYPT_SILENT = 64;

    int X509_ASN_ENCODING = 0x00000001;
    int PKCS_7_ASN_ENCODING = 0x00010000;

    int AT_KEYEXCHANGE = 1;

    int CERT_FIND_PUBLIC_KEY = 393216;

    int ALG_CLASS_HASH = 32768;
    int HP_HASHVAL = 2;
    int PP_KEYEXCHANGE_PIN = 0x20;


    boolean CryptAcquireContextA(
            PointerByReference hprov,
            String pszContainer,
            String pszProvider,
            int dwProvType,
            Integer dwFlags);

    boolean CryptReleaseContext(
            PointerByReference prov,
            int dwFlags
    );

    boolean CryptGetProvParam(
            Pointer prov,
            int dwParam,
            byte[] pbData,
            IntByReference dwDataLen,
            int dwFlags
    );

    boolean CryptExportPublicKeyInfo(
            Pointer hCryptProv,
            int dwKeySpec,
            int dwCertEncodingType,
            CERT_PUBLIC_KEY_INFO pInfo,
            IntByReference pcbInfo
    );

    Pointer CertOpenSystemStoreA(
            Pointer hprov,
            String szSubsystemProtocol
    );

    PCERT_CONTEXT.ByValue CertFindCertificateInStore(
            Pointer hCertStore,
            int dwCertEncodingType,
            int dwFindFlags,
            int dwFindType,
            CERT_PUBLIC_KEY_INFO.ByReference pvFindPara,
            Pointer pPrevCertContext
    );

    boolean CryptCreateHash(
            Pointer hProv,
            int AlgId,
            Pointer hKey,
            int dwFlags,
            PointerByReference phHash
    );

    boolean CryptHashData(
            Pointer hHash,
            byte[] pbData,
            int dwDataLen,
            int dwFlags
    );

    boolean CryptSignHashA(
            Pointer hHash,
            int dwKeySpec,
            String sDescription,
            int dwFlags,
            byte[] pbSignature,
            IntByReference pdwSigLen
    );

    boolean CryptGetHashParam(
            Pointer hHash,
            int dwParam,
            byte[] pbData,
            IntByReference pdwDataLen,
            int dwFlags
    );

    boolean CryptSetProvParam(
            Pointer prov,
            int dwParam,
            byte[] pbData,
            int dwFlags
    );
}
