package ru.dmartynov.gost.security.nativ.structures;

import com.sun.jna.Pointer;
import com.sun.jna.Structure;

import java.util.Arrays;
import java.util.List;

public class CRYPT_BIT_BLOB extends Structure {
    public int cbData;
    public Pointer pbData;
    public int cUnusedBits;

    public CRYPT_BIT_BLOB() {
    }

    protected List<?> getFieldOrder() {
        return Arrays.asList("cbData", "pbData", "cUnusedBits");
    }

    public static class ByReference extends CRYPT_BIT_BLOB implements Structure.ByReference {
    }

    ;
}