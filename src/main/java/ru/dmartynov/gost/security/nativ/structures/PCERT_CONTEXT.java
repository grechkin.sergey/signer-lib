package ru.dmartynov.gost.security.nativ.structures;

import com.sun.jna.Structure;

import java.util.Arrays;
import java.util.List;

public class PCERT_CONTEXT extends Structure {
    public CERT_CONTEXT.ByReference certContext;

    public PCERT_CONTEXT() {
    }

    protected List<?> getFieldOrder() {
        return Arrays.asList("certContext");
    }

    public static class ByReference extends PCERT_CONTEXT implements Structure.ByReference {
    }

    public static class ByValue extends PCERT_CONTEXT implements Structure.ByValue {
    }
}